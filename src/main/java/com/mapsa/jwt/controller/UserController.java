package com.mapsa.jwt.controller;

import com.mapsa.jwt.dto.AuthResponseDto;
import com.mapsa.jwt.dto.LoginRequestDto;
import com.mapsa.jwt.model.User;
import com.mapsa.jwt.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping(value = "/register")
    public void register(@RequestBody User user){
        userService.register(user);

    }
    @GetMapping(value = "/users")
    public List<User> getUsers(){
        return userService.getUsers();
    }

    @PostMapping(value = "/login")
    public AuthResponseDto login(@RequestBody LoginRequestDto loginRequestDto){
        return userService.login(loginRequestDto) ;
    }
}
