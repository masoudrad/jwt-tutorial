package com.mapsa.jwt.model;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "_User")
@Table(name = "USER_TBL")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private  String name;

    private String password;

}
