package com.mapsa.jwt.service;

import com.mapsa.jwt.UserPrincipal;
import com.mapsa.jwt.dto.AuthResponseDto;
import com.mapsa.jwt.dto.LoginRequestDto;
import com.mapsa.jwt.model.User;
import com.mapsa.jwt.repo.UserRepository;
import com.mapsa.jwt.security.JwtProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsServiceImpl userDetailsService;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;

    public List<User> getUsers(){
        return userRepository.findAll();
    }

    public void register(User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public AuthResponseDto login(LoginRequestDto loginRequestDto){


        authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequestDto.getName(),
                        loginRequestDto.getPassword()));
        UserPrincipal userPrincipal = userDetailsService.loadUserByUsername(loginRequestDto.getName());
        String token = jwtProvider.generateToken(userPrincipal);
        return new AuthResponseDto(loginRequestDto.getName(),token);

    }


}
