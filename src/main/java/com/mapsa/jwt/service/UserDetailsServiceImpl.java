package com.mapsa.jwt.service;

import com.mapsa.jwt.UserPrincipal;
import com.mapsa.jwt.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;
    @Override
    public UserPrincipal loadUserByUsername(String s) throws UsernameNotFoundException {
        return new UserPrincipal(userRepository.findByName(s));
    }
}
