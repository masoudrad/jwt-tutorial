package com.mapsa.jwt.security;

import com.mapsa.jwt.UserPrincipal;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtProvider {
    private static final String SECRET_KEY = "SecretSecretSecretSecretSecretSecretSecretSecret" ;

    public String generateToken(UserPrincipal userPrincipal) {
        Map<String,?>  payloadMap =  new HashMap<>();
        return create(payloadMap,userPrincipal.getUsername());

    }

    private String create(Map<String,?> payload, String subject){
        return Jwts
                .builder()
                .setClaims(payload)
                .setSubject(subject)
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(new Date(System.currentTimeMillis()+(60*60*2)))
                .signWith(SignatureAlgorithm.HS256,SECRET_KEY).compact();
    }

//    Check Token that get from Client
    private Claims extractAllClaims(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(SECRET_KEY)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private <T> T extractClaim(String token, Function<Claims, T> func) {
        Claims claims = extractAllClaims(token);
        return func.apply(claims);
    }

    public String getUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }
    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(Date.from(Instant.now()));
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }
    public Boolean validateToken(String token, UserDetails userDetails) {
        String username = getUsername(token);
        return (username.equals(userDetails.getUsername()) &&
                !isTokenExpired(token));
    }

}
