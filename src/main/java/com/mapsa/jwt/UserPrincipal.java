package com.mapsa.jwt;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.List;

public class UserPrincipal extends User {
    private static final GrantedAuthority GRANTED_AUTHORITY = new GrantedAuthority() {
        @Override
        public String getAuthority() {
            return "USER";
        }
    };
    public UserPrincipal(com.mapsa.jwt.model.User user) {
        super(user.getName(), user.getPassword(), List.of(GRANTED_AUTHORITY));
    }
}
